import numpy as np
import pandas as pd
from datetime import date, datetime

from requests import Session
from config.Config import mysql_get_mydb
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import os
from endpoints.user_api import get_db
from helpers.helpers import get_data_location
from typing import List
from fastapi import Depends, Query
from config.sqlquery import get_timesheet_query
 
 
from models.models import HealthArea, HealthAreaBaseListe
from models.validation_crud import validated_ids, validation_count
from endpoints.pyramid_api import Health_area
 
person_attributes = [
    'firstname', 'surname', 'othername', 'gender',
    'birth_date', 'dependents','marital_status',
    'mobile_phone', 'address', 'ref_engagement',
    'year_of_appointment', 'matricule', 'ref_on_employment',
    'degree','position', 'cadre', 'classification',
    'job', 'salary_grade', 'salaire', 'prime', 'identifie'
]
   
def download_data(pr_id: str, tr_id: str, zs_id: str, fosa_id: str, filter: str, db: Session = Depends(get_db)):
    data = get_internal_completeness_data(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id )

    title = ''
    if filter == 'all':
        title = 'AGENTS'
    elif filter == "admin":
        data = data.loc[data['cadre'] == 'ADMINISTRATIF']
        title = 'ADMINISTRATIF'
    elif filter == 'prosante':
        data = data.loc[data['cadre'] != 'ADMINISTRATIF']
        title = 'PRO SANTE'
    elif filter == 'activite':
        data = data.loc[data['position'] == "Activité"]
        title = 'AGENTS EN POSITION ADM. ACTIVE'
    
    elif filter == 'admis_ss':
        data = data.loc[data['matricule'].str.len() > 3]
        title = 'AGENTS ADMIS SOUS STATUT'
    elif filter == 'nu':
        data = data.loc[data['matricule'].str.len() <= 3]
        title = 'AGENTS NOUVELLES UNITES'
    elif filter == 'eligible_retraite':
        data = eligible_retraite(data)
        title = 'ELIGIBLE A LA RETRAITE'

    elif filter == 'retraite':
        data = data.loc[data['position'] == "Retraité"]
        title = 'AGENTS RETRAITES'
    
    elif filter == 'dead':
        data = data.loc[data['position'] == "Décédé"]
        title = 'AGENTS DECEDES'

    elif filter == 'validated':
        ids  = validated_ids(
            facility=fosa_id,
            healthArea= zs_id,
            county=tr_id,
            district=pr_id,
            db=db
        )
        data = data.loc[data['id'].isin(ids)]
        title = 'AGENTS VALIDES'
    
    

    data = data.reset_index()
    data = data.rename(columns={"index": "No"})
    data.insert(
        loc=7, column='lien',
        value="https://drc.ihris.org/index.php/view?id="+
        data['id']
    )
    data['No'] = range(1, len(data) + 1)
    return data, f"{title}"

def eligible_retraite(data):

    data1 = data.loc[data['birth_date_y'] <= date.today().year - 65]
    data2 = data.loc[data['year_of_appointment_y'] <= date.today().year - 30]
    coll = pd.concat([data1, data2])
    coll = coll.drop_duplicates()
    coll['anciennete'] = date.today().year - coll['year_of_appointment_y']
    coll['age'] = date.today().year - coll['birth_date_y']
    return coll

def get_dashboard_data(pr_id: str, tr_id: str, zs_id: str, fosa_id: str, db: Session = Depends(get_db)):

    data = get_internal_completeness_data(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id)
    
    effectif = np.int64(data['id'].count()).item()
    """Recrutement staff"""
    x_recrument = []
    y_recrument = []
    i = 1970
    while i <= date.today().year:
        x_recrument.append(
            np.int16(data['id'].loc[data['year_of_appointment_y'] == i].count()).item())
        # if i % 2 == 0:
            
        # else:

        #y_recrument.append('.')
        i += 1

        y_recrument.append(i)
    """Statistic by cadre"""
    cadre_data = data.groupby(['cadre'])['id'].count()
    cadre_data = cadre_data.sort_values(ascending=False)
    """Statistic by position """
    position_data = data.groupby(['position'])['id'].count()

    """Statictic by degree"""
    degree_data = data.groupby(['degree'])['id'].count()
    degree_data = degree_data.sort_values(ascending=False)

    """ STATISTIC BY FACILITY"""
    datalocation = get_data_location(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id, zs_filter=True)
 
    data.rename(columns={datalocation: "location"}, inplace=True)

    facility_data = data.groupby(['location','health_area_id'])['id'].count()

    
    facility_data_base_list = getHealthAreaBaseListe(db=db, pr_id=pr_id, zs_id= zs_id, staff=effectif,  groupData=facility_data) if pr_id == "district|2" else []


    """ Gender """
    gender_m = np.int16(
        data.loc[data['gender'] == "gender|M"]['id'].count()).item()
    gender_f = np.int16(
        data.loc[data['gender'] == "gender|F"]['id'].count()).item()
    admin = np.int64(
        data.loc[data['cadre'] == 'ADMINISTRATIF']['id'].count()).item()
    
    position_active = np.int64(
        data.loc[data['position'] == "Activité"]['id'].count()).item()
    
    position_retraite = np.int64(
        data.loc[data['position'] == "Retraité"]['id'].count()).item()
    
    position_dead = np.int64(
        data.loc[data['position'] == "Décédé"]['id'].count()).item()
    

    
    timesheet = timeshee_dashboard(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id, effectif=effectif)
    ''' Employment statuts'''
    nu = np.int16(data.loc[data['matricule'].str.contains(
        'N') == True]['id'].count()).item()
    adminsoustatut = np.int16(
        data.loc[data['matricule'].str.len() > 3]['id'].count()).item()
    
   
    val = validation_count(
         facility=fosa_id,
         healthArea=zs_id ,
         county=tr_id,
         district=pr_id,
         db=db
     )
    return {
        'validated': val,
        "recrutement": {
            'labels': y_recrument,
            'data': x_recrument
        },
        "cadre": {
            "labels": cadre_data.index.tolist(),
            "data": cadre_data.values.tolist(),
            "max": max(cadre_data.values.tolist(),default=0)

        },
        "position": {
            "labels": position_data.index.tolist(),
            "data": position_data.values.tolist(),
            "max": max(position_data.values.tolist(),default=0)
        },
        "degree": {
            "labels": degree_data.index.tolist(),
            "data": degree_data.values.tolist(),
             "max": max(degree_data.values.tolist(), default=0)
        },
        "gender": {
            'm': gender_m,
            'f': gender_f
        },
        "facility_data": facility_data_base_list,
        "effectif": effectif,
        "admin": admin,
        "position_active":  position_active,
        "timesheet": timesheet,
        "effectif_nu": nu,
        "effectif_adminSS": adminsoustatut,
        "agecurve": getAgecurveAdmin_statut(data=data),
        "general_agecurve":getAgecurveGeneral(data),
        "eligible_retaite": len(eligible_retraite(data)),
        "retraite": position_retraite,
        "dead" : position_dead
    }

def getAgecurveAdmin_statut(data):
    x = []
    x2 = []
    y = []
    i = date.today().year
    m = 1920
    while i % 5 != 0:
        i -= 1
    current_year = i
    while i > m:
        mdata = data.loc[data['birth_date_y'].between(i-4, i) == True]
        val = np.int16(
            mdata.loc[mdata['matricule'].str.len() > 3]['id'].count()).item()
        nu = np.int16(
            mdata.loc[mdata['matricule'].str.contains('N') == True]['id'].count()).item()
        x.append(val)
        x2.append(nu)
        y.append(str(current_year-i) + ' - ' + str(current_year-i+5))
        i -= 5
    return {
        'values': {'admis_statut': x, 'nu': x2},
        'max': max([max(x),max(x2)]), 'labels': y,
        '_values': {'admis_statut': x[::-1], 'nu': x2[::-1]},
        '_max': max([max(x),max(x2)]),
        '_labels': y[::-1]
    }


def getAgecurveGeneral(data):
    x = []
    x2 = []
    y = []
    z = []
    i = date.today().year
    m = 1920
    while i % 5 != 0:
        i -= 1
    current_year = i
    while i > m:
        mdata = data.loc[data['birth_date_y'].between(i-4, i) == True]
        mal = np.int16( mdata.loc[mdata['gender'] == "gender|M"]['id'].count()).item()
        fem = np.int16( mdata.loc[mdata['gender'] == "gender|F"]['id'].count()).item()
        x.append(mal)
        x2.append(fem)
        y.append(str(current_year-i) + ' - ' + str(current_year-i+5) )
        z.append(str(i-4) + ' ' + str(i))
        i -= 5
    return {
        'values': {'mal': x, 'fem': x2},
        'max': max([max(x),max(x2)]),
        'labels': y,
        '_values': {'mal': x[::-1], 'fem': x2[::-1]},
        '_max': max([max(x),max(x2)]),
        '_labels': y[::-1]
    }


def timeshee_dashboard(pr_id: str, tr_id: str, zs_id: str, fosa_id: str, effectif: int):
    data = get_timesheet_data(pr_id=pr_id, tr_id=tr_id,
                              zs_id=zs_id, fosa_id=fosa_id)
    effectif_actif = data.drop_duplicates(subset=['parent'])['parent'].count()

    x = []
    x2 = []
    y = []
    i = 1
    d = date.today() - relativedelta(months=18)
    while i <= 18:
        str = d.strftime("%Y-%m")
        taux_general = 0
        toux_effecti_actif = 0
        timesheet_count = int(np.int16(
            data['id'].loc[data['mois_annee'].str.contains(str) == True].count()).item())
        if effectif > 0:
            taux_general = int(timesheet_count / effectif * 100)
        if effectif_actif > 0:
            toux_effecti_actif = int(
                timesheet_count / effectif_actif * 100)
        x.append(taux_general if taux_general <= 100 else 100)
        x2.append(toux_effecti_actif if toux_effecti_actif <= 100 else 100)

        y.append(d.strftime("%b-%y"))
        i += 1
        d = d + relativedelta(months=1)
    return {
        'labels': y,
        'data': {
            'taux_general': x,
            'taux_actif': x2
        }
    }


def get_internal_completeness_report_career(
        pr_id: str = "0", tr_id: str = "0", zs_id: str = "0", fosa_id: str = "0", filter: int = 0
):
    data = get_internal_completeness_data()
    selectedKey = 'district'
    subKey = "county"
    selectedkeyLabel = 'county_name'
    subKey = None
    if filter == 0:
        if fosa_id != "0":
            selectedKey = 'facility'
            subKey = "facility"
            data = data.loc[data[selectedKey] == fosa_id]
            selectedkeyLabel = 'fullname'

        elif zs_id != "0":
            selectedKey = 'health_area_id'
            subKey = "facility"
            data = data.loc[data[selectedKey] == zs_id]
            selectedkeyLabel = 'facility_name'
        elif tr_id != "0":
            selectedKey = 'county'
            subKey = "health_area_id"
            data = data.loc[data[selectedKey] == tr_id]
            selectedkeyLabel = 'health_area_name'
        elif (pr_id != '0'):
            selectedKey = 'district'
            subKey = "county"
            data = data.loc[data[selectedKey] == pr_id]
            selectedkeyLabel = 'county_name'
        else:
            selectedkeyLabel = 'district_name'
            subKey = "district"

    else:
        if pr_id != "0":
            selectedKey = 'district'
            subKey = "county"
            data = data.loc[data[selectedKey] == pr_id]

        selectedkeyLabel = 'health_area_name'
        subKey = "health_area_id"

    data[['id']] = data[['id']].notnull()
    
    data.rename(columns={selectedkeyLabel: "LOCATION"}, inplace=True)
    datakays = person_attributes
    data[datakays] = data[datakays].isnull()

    # Create pivot table
    data.rename(columns={subKey: "location_id"}, inplace=True)
     
    pivot = np.round(pd.pivot_table(
        data, index=['LOCATION', 'location_id'], aggfunc=np.sum, fill_value=0,), 2)

    if (len(pivot)):
        pivot['TOTAL'] = pivot.loc[:, datakays].sum(axis=1)
        pivot['TAUX'] = 100 - \
            (pivot['TOTAL'] / (len(datakays) * pivot['id']) * 100)
    return pivot

def get_salaire_prime_report(pr_id: str = "0", tr_id: str = "0", zs_id: str = "0",
    fosa_id: str = "0", zs_filter: int = 0):

    people = get_internal_completeness_data(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id)
   
    selectedLocation = None
    if zs_filter == 0:
        if fosa_id != "0":
            selectedLocation = 'fullname'
        elif zs_id != "0":
            selectedLocation = 'facility_name'
        elif tr_id != "0":
            selectedLocation = 'health_area_name'
        elif (pr_id != '0'):
            selectedLocation = 'county_name'
        else:
            selectedLocation = 'district_name'
    else:
        if pr_id != "0":
            selectedLocation = 'health_area_name'
        else:
            selectedLocation = 'district_name'

    people.rename(columns={selectedLocation: "location"}, inplace=True)
    people['effectif'] = 1
    #salaire_et_prime
    people['salaire_et_prime'] =  (people["salaire"] ==1) & (people["prime"]== 1)
    people['salaire_et_prime'] = people['salaire_et_prime'].astype(int)
    #salaire_ou_prime
    people['salaire_ou_prime'] =  (people["salaire"] ==1) | (people["prime"]== 1)
    people['salaire_ou_prime'] = people['salaire_ou_prime'].astype(int)
    # in_salaire_in_prime
    people['in_salaire_in_prime'] =  (people["salaire"] ==0) & (people["prime"]== 0)
    people['in_salaire_in_prime'] = people['in_salaire_in_prime'].astype(int)

    report = people.groupby(["location"])['effectif',
            'salaire', 'prime','salaire_et_prime', 
            'salaire_ou_prime','in_salaire_in_prime'].sum()
    print(report)
    return report

"""

 Timesheet Report

"""
def performance_bonus_calc(
    pr_id: str = "0", tr_id: str = "0", zs_id: str = "0",
    fosa_id: str = "0", zs_filter: int = 0,
    start_date: str = None, end_date: str = None,
    download: int = 0
):

    timesheets = get_timesheet_data(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id)
    people = get_internal_completeness_data(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id)
    timesheets = timesheets.fillna(0)
    """ Timesheet calc"""

    dates = []
    dates_local_abb = []
    i = 0
    dates_len = 0
    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.strptime(end_date, "%Y-%m-%d")
    cmIkeys = ['salaire_recu', 'prime_risque',
               'prime_locale', 'prime_ptf']

    '''Start  loop '''
    columns = []
    intComLables = []
    intComSumColumns = []

    while i <= 14:
        _date = start_date + relativedelta(months=i)
        key = _date.strftime("%Y-%m")
        dates_local_abb.append({
            'key': key,
            'local_abb': _date.strftime("%b-%y")
        })
        dates.append(key)
        columns.append(key)

        timesheets[key] = kkFilter = timesheets["mois_annee"].str.contains(key)

        for elt in cmIkeys:
            m = f'{key}_{elt}'
            columns.append(m)
            intComLables.append(m)
            timesheets[m] = timesheets[kkFilter == True][elt]

        _intComColumn = f'{key}_sum_com'
        columns.append(_intComColumn)
        intComSumColumns.append(_intComColumn)

        timesheets[_intComColumn] = (
            (len(cmIkeys) - (timesheets[kkFilter == True][cmIkeys] == 0.0).sum(axis=1)) +
            (timesheets[kkFilter == True]['prime'] == 0).astype(int) + 
            (timesheets[kkFilter == True]['salaire'] == 0).astype(int) +
            (timesheets[kkFilter == True]['health_area_name'].str.contains('DPS') == True ).astype(int) +
            (timesheets[kkFilter == True]['health_area_name'].str.contains('IPS') == True ).astype(int) 
           
        )

        timesheets = timesheets.drop_duplicates(
            subset=['parent', 'mois_annee'])
        i += 1
        dates_len += 1

        ''' loop break '''
        if (end_date.strftime("%Y-%m") == _date.strftime("%Y-%m")) or i >=12:
            break

    '''End loop '''

    datalocation = get_data_location(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id, zs_filter=zs_filter)
 
    timesheets.rename(columns={datalocation: "location"}, inplace=True)
    timesheet_report = timesheets.groupby(
        ['location'])[columns].sum()
    timesheet_index = timesheet_report.index

    effectif_general = (people[people[datalocation].isin(timesheet_index)]
                        .groupby(datalocation, sort=False))["id"].count()

    dataactiveEffec = timesheets.drop_duplicates(subset=['parent'])
    dfactif = (dataactiveEffec[dataactiveEffec['location'].isin(
        timesheet_index)].groupby(['location'], sort=False))["id"].count()
    
    people[person_attributes] = people[person_attributes].notnull()
    people['total_career'] = people[person_attributes].sum(axis=1)

    career_data_comp  = (people[people[datalocation].isin(timesheet_index)]
                        .groupby(datalocation, sort=False))["total_career"].sum()

    timesheet_report['effectif_gen'] = effectif_general
    timesheet_report['effectif_actif'] = dfactif

    timesheet_report['timesheet_sum'] = timesheet_report[dates].agg('sum', axis=1)
    timesheet_report[intComLables].agg('sum', axis=1)
    timesheet_report['timesheet_average'] = timesheet_report['timesheet_sum']/dates_len
    timesheet_report['timesheet_max'] = timesheet_report['effectif_gen']  * dates_len 
    timesheet_report['timesheet_perc_gen'] =  timesheet_report['timesheet_sum'] / timesheet_report['timesheet_max']  *100
    timesheet_report['timesheet_perc_actif'] = timesheet_report['timesheet_sum'] / (timesheet_report['effectif_actif'] *  dates_len ) *100
    timesheet_report['timesheet_com_int_sum'] = timesheet_report[intComSumColumns].agg(
        'sum', axis=1)
    
    timesheet_report['timesheet_com_int_max'] = (timesheet_report['effectif_actif'] *  dates_len * len(cmIkeys))
    
    timesheet_report['timesheet_com_int_len'] = len(intComLables)
    timesheet_report['timesheet_com_int_perc'] = (timesheet_report['timesheet_com_int_sum'] /
                                        timesheet_report['timesheet_com_int_max'] * 100)

    timesheet_report['career_data_comp'] = career_data_comp
    timesheet_report['career_data_comp_max'] = len(person_attributes)*timesheet_report['effectif_gen']
    timesheet_report['career_data_comp_perc'] =  (timesheet_report['career_data_comp']/timesheet_report['career_data_comp_max'])*100

    
    timesheet_report['perfomance_bonus_com_career'] = 0 #(timesheet_report['career_data_comp_perc'] * 30/100)
    timesheet_report['perfomance_bonus_com_int_timesheet'] = 0 # (timesheet_report['timesheet_com_int_perc'] * 30/100)
    timesheet_report['perfomance_bonus_com_timesheet'] = 0# (timesheet_report['timesheet_perc_gen'] * 30/100)
    
    timesheet_report.loc[timesheet_report['effectif_gen'] <151,'perfomance_bonus_com_career'] = (timesheet_report.loc[timesheet_report['effectif_gen']<151]['career_data_comp_perc']) * 50/100/100*30
    timesheet_report.loc[timesheet_report['effectif_gen'] <151,'perfomance_bonus_com_int_timesheet'] = (timesheet_report.loc[timesheet_report['effectif_gen']<151]['timesheet_com_int_perc']) * 50/100/100*30
    timesheet_report.loc[timesheet_report['effectif_gen'] <151,'perfomance_bonus_com_timesheet'] = (timesheet_report.loc[timesheet_report['effectif_gen']<151]['timesheet_perc_gen']) * 50/100/100* 40
    
    timesheet_report.loc[(timesheet_report['effectif_gen'] >150) & (timesheet_report['effectif_gen'] <251) , 'perfomance_bonus_com_career'] = (timesheet_report.loc[(timesheet_report['effectif_gen'] >150) & (timesheet_report['effectif_gen'] <251) ]['career_data_comp_perc']) * 70/100/100* 30
    timesheet_report.loc[(timesheet_report['effectif_gen'] >150) & (timesheet_report['effectif_gen'] <251) , 'perfomance_bonus_com_int_timesheet'] = (timesheet_report.loc[(timesheet_report['effectif_gen'] >150) & (timesheet_report['effectif_gen'] <251) ]['timesheet_com_int_perc']) * 70/100/100* 30
    timesheet_report.loc[(timesheet_report['effectif_gen'] >150) & (timesheet_report['effectif_gen'] <251) , 'perfomance_bonus_com_timesheet'] = (timesheet_report.loc[(timesheet_report['effectif_gen'] >150) & (timesheet_report['effectif_gen'] <251) ]['timesheet_perc_gen']) * 70/100/100* 40

    timesheet_report.loc[ (timesheet_report['effectif_gen'] >250) & (timesheet_report['effectif_gen'] <351) , 'perfomance_bonus_com_career'] = (timesheet_report.loc[(timesheet_report['effectif_gen'] >250) & (timesheet_report['effectif_gen'] <351) ]['career_data_comp_perc']) * 90/100/100* 30
    timesheet_report.loc[ (timesheet_report['effectif_gen'] >250) & (timesheet_report['effectif_gen'] <351) , 'perfomance_bonus_com_int_timesheet'] = (timesheet_report.loc[(timesheet_report['effectif_gen'] >250) & (timesheet_report['effectif_gen'] <351) ]['timesheet_com_int_perc']) * 90/100/100* 30
    timesheet_report.loc[ (timesheet_report['effectif_gen'] >250) & (timesheet_report['effectif_gen'] <351) , 'perfomance_bonus_com_timesheet'] = (timesheet_report.loc[(timesheet_report['effectif_gen'] >250) & (timesheet_report['effectif_gen'] <351) ]['timesheet_perc_gen']) * 90/100/100* 40

    timesheet_report.loc[timesheet_report['effectif_gen'] >350,'perfomance_bonus_com_career'] = (timesheet_report.loc[timesheet_report['effectif_gen']>350]['career_data_comp_perc']) * 100/100/100*30
    timesheet_report.loc[timesheet_report['effectif_gen'] >350,'perfomance_bonus_com_int_timesheet'] = (timesheet_report.loc[timesheet_report['effectif_gen']>350]['timesheet_com_int_perc']) * 100/100/100*30
    timesheet_report.loc[timesheet_report['effectif_gen'] >350,'perfomance_bonus_com_timesheet'] = (timesheet_report.loc[timesheet_report['effectif_gen']>350]['timesheet_perc_gen']) * 100/100/100* 40
    
    timesheet_report['perfomance_bonus_total'] = timesheet_report[['perfomance_bonus_com_career','perfomance_bonus_com_int_timesheet','perfomance_bonus_com_timesheet']].sum(axis=1)
    timesheet_report[['perfomance_bonus_com_career','perfomance_bonus_com_int_timesheet','perfomance_bonus_com_timesheet','perfomance_bonus_total']] =  timesheet_report[['perfomance_bonus_com_career','perfomance_bonus_com_int_timesheet','perfomance_bonus_com_timesheet','perfomance_bonus_total']]
    

   # timesheet_report = timesheet_report.drop('ZS PAL')
    subtotal = timesheet_report.sum()

    timesheet_report['row_number'] = timesheet_report.reset_index().index

    subtotal[['timesheet_perc_actif','timesheet_perc_gen',
            'career_data_comp_perc','timesheet_com_int_perc']] = subtotal[[
            'timesheet_perc_actif','timesheet_perc_gen',
            'career_data_comp_perc','timesheet_com_int_perc']
            ]/  timesheet_report.shape[0]
    if(download ==  0):
         timesheet_report.loc['TOTAL'] = subtotal

    subtotal['bonus_ips_comp_timesheet'] = 40/100 * subtotal['timesheet_perc_gen'] 
    subtotal['bonus_dps_comp_timesheet'] = 80/100 * subtotal['timesheet_perc_gen'] 

    subtotal['bonus_ips_career'] = 30/100 * subtotal['career_data_comp_perc'] 
    subtotal['bonus_dps_career'] = 60/100 * subtotal['career_data_comp_perc'] 

    subtotal['bonus_ips_int_timesheet'] = 30/100 * subtotal['timesheet_com_int_perc'] 
    subtotal['bonus_dps_int_timesheet'] = 60/100 * subtotal['timesheet_com_int_perc'] 
    
    return timesheet_report, dates_local_abb, subtotal


def completude_timesheet(
    pr_id: str = "0", tr_id: str = "0", zs_id: str = "0",
    fosa_id: str = "0", zs_filter: int = 0,
    start_date: str = None, end_date: str = None,
    download_details: int = 0
):

    timesheets = get_timesheet_data(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id)

    people = get_internal_completeness_data(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id)
    timesheets = timesheets.fillna(0)
    """ Timesheet calc"""

    dates = []
    dates_local_abb = []
    i = 0
    dates_len = 0
    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.strptime(end_date, "%Y-%m-%d")
    cmIkeys = ['salaire_recu', 'prime_risque',
               'prime_locale', 'prime_ptf']

    '''Start  loop '''
    columns = []
    intComLables = []
    intComSumColumns = []
    while i <= 24:
        _date = start_date + relativedelta(months=i)
        key = _date.strftime("%Y-%m")
        dates_local_abb.append({
            'key': key,
            'local_abb': _date.strftime("%b-%y")
        })
        dates.append(f'{key}')
        columns.append(f'{key}')

        timesheets[key] = kkFilter = timesheets["mois_annee"].str.contains(key)

        for elt in cmIkeys:
            m = f'{key}_{elt}'
            columns.append(m)
            intComLables.append(m)
            timesheets[m] = timesheets[kkFilter == True][elt]

        _intComColumn = f'{key}_sum_com'
        columns.append(_intComColumn)
        intComSumColumns.append(_intComColumn)
        timesheets[_intComColumn] = (
            (len(cmIkeys) - (timesheets[kkFilter == True][cmIkeys] == 0.0).sum(axis=1)) +
            (timesheets[kkFilter == True]['prime'] == 0).astype(int) + 
            (timesheets[kkFilter == True]['salaire'] == 0).astype(int) +
            (timesheets[kkFilter == True]['health_area_name'].str.contains('DPS') == True ).astype(int) +
            (timesheets[kkFilter == True]['health_area_name'].str.contains('IPS') == True ).astype(int)     
        )
        timesheets = timesheets.drop_duplicates(
            subset=['parent', 'mois_annee'])
        i += 1
        dates_len += 1

        ''' loop break '''
        if end_date.strftime("%Y-%m") == _date.strftime("%Y-%m"):
            break

    '''End loop '''

    datalocation = get_data_location(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id, zs_filter=zs_filter)

    if download_details == 2:

        timesheet_report = timesheets.groupby(
            ['fullname', 'facility_name', 'health_area_name','parent','prime','salaire'])[columns].sum()
        timesheet_index = timesheet_report.index
        timesheet_report.insert(
            loc=0, column='fosa', value=timesheet_index.get_level_values(1))
        timesheet_report.insert(
            loc=0, column='health_area_name', value=timesheet_index.get_level_values(2))
        timesheet_report.insert(
            loc=0, column='fullname', value=timesheet_index.get_level_values(0))
        timesheet_report.insert(
            loc=3, column='lien',
            value="https://drc.ihris.org/index.php/view?id="+ timesheet_index.get_level_values(3)
        )
        timesheet_report.insert(
            loc=4, column='Prime_risque',
            value= timesheet_index.get_level_values(4)
        )
        timesheet_report.insert(
            loc=4, column='Salaire',
            value= timesheet_index.get_level_values(5)
        )
        
        

        return timesheet_report

    timesheets.rename(columns={datalocation: "location"}, inplace=True)
    timesheet_report = timesheets.groupby(
        ['location'])[columns].sum()
    timesheet_index = timesheet_report.index
    effectif_general = (people[people[datalocation].isin(timesheet_index)]
                        .groupby(datalocation, sort=False))["id"].count()

    dataactiveEffec = timesheets.drop_duplicates(subset=['parent'])
    dfactif = (dataactiveEffec[dataactiveEffec['location'].isin(
        timesheet_index)].groupby(['location'], sort=False))["id"].count()

    timesheet_report['effectif_gen'] = effectif_general
    timesheet_report['effectif_actif'] = dfactif
    timesheet_report['sum'] = timesheet_report[dates].agg('sum', axis=1)

    timesheet_report['com_int_sum'] = timesheet_report[intComSumColumns].agg(
        'sum', axis=1)

    timesheet_report['com_int_max'] = timesheet_report['effectif_actif'] * \
        dates_len * len(cmIkeys)
    timesheet_report['com_int_len'] = len(intComLables)
    timesheet_report['com_int_perc'] = (timesheet_report['com_int_sum'] /
                                        timesheet_report['com_int_max'] * 100)

    timesheet_report[intComLables].agg(
        'sum', axis=1)
    timesheet_report['moyenne'] = timesheet_report['sum']/dates_len
    timesheet_report['report_perc_gen'] =  timesheet_report['moyenne']/ timesheet_report['effectif_gen'] * 100
    timesheet_report['report_perc_actif'] =  timesheet_report['moyenne']/ timesheet_report['effectif_actif'] * 100

    #if zs_filter == 1 : 
     #   timesheet_report = timesheet_report.drop('ZS PAL') 

    subtotal = timesheet_report.sum()

    subtotal[['report_perc_gen','report_perc_actif',
             'com_int_perc']] = subtotal[[
             'report_perc_gen','report_perc_actif',
             'com_int_perc']
             ]/  timesheet_report.shape[0]

    return timesheet_report, dates_local_abb, subtotal

'''
revenvus_report_data

'''
def revenvus_report_data(
    pr_id: str = "0", tr_id: str = "0", zs_id: str = "0",
    fosa_id: str = "0", zs_filter: int = 0,
    start_date: str = None, end_date: str = None,
    report_type: int = 0,
    cadres_ids: List[str] = Query(None),
    jobs_ids: List[str] = Query(None),
    class_ids: List[str] = Query(None),
    select_option:  List[str] = Query(None)
):

    timesheets = get_timesheet_data(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id)

    people = get_internal_completeness_data(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id)

    datalocation = get_data_location(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id, zs_filter=zs_filter)

    revenu_report = people.groupby(datalocation, sort=False). count()
    revenu_report['_location'] = revenu_report.index

    effectifActifData = timesheets.drop_duplicates(subset=['parent'])

    dfactif = (effectifActifData[effectifActifData[datalocation].isin(
        revenu_report.index)].groupby(datalocation, sort=False))["id"].count()
    dfactif.index.name = "effecti_actif"

    """ Timesheet calc"""

    if start_date == None or end_date == None:
        start_date = date.today() - relativedelta(months=6)
        end_date = date.today()
    else:
        start_date = datetime.strptime(start_date, "%Y-%m-%d")
        end_date = datetime.strptime(end_date, "%Y-%m-%d")

    dates = ['id']
    dates_local_abb = []
    dates = []
    dataIndex = ['_location', 'effectif', 'effectif_actif']
    i = 0
    dates_len = 0

    '''Start  loop '''

    while i <= 9:
        _date = start_date + relativedelta(months=i)
        key = _date.strftime("%y-%m_montant")
        keyfilter = _date.strftime("%Y-%m")
        key2 = _date.strftime("%Y-%m_rapport")
        dates.append(key)
        dataIndex += [key, key2]

        dates_local_abb.append({
            'key': key,
            'local_abb': _date.strftime("%b-%y"),
            'effectif': key2
        })

        mdata = timesheets[timesheets[datalocation].isin(revenu_report.index) & (
            timesheets['mois_annee'].str.contains(keyfilter) == True)]
        mdata = mdata.drop_duplicates(['parent'])

        '''Cadre, job, classification filters '''

        if len(cadres_ids) > 0:
            mdata = mdata[mdata['cadre'].isin(cadres_ids)]

        if len(jobs_ids) > 0:
            mdata = mdata[mdata['job'].isin(jobs_ids)]

        if len(class_ids) > 0:
            mdata = mdata[mdata['classification'].isin(class_ids)]

        mdata['total'] = mdata[select_option].sum(axis=1)
        revenu_report[key] = mdata.groupby(datalocation)[['total']].sum()
        revenu_report[key2] = mdata.groupby(
            datalocation)[['parent']].count()

        i = i + 1
        dates_len += 1
        if keyfilter == (date.today() - relativedelta(months=1)).strftime("%Y-%m"):
            break

    revenu_report = revenu_report.rename(columns={"id": "effectif"})
    revenu_report["effectif_actif"] = dfactif
    revenu_report.index.name = "location"
    revenu_report['total'] = revenu_report[dates].agg('sum', axis=1)
    revenu_report['moyenne'] = (revenu_report['total'] / dates_len).astype(int)
    dataIndex += ['total', 'moyenne']

    return revenu_report[dataIndex], dates_local_abb


'''
 get_internal_completeness_data
'''


def get_internal_completeness_data(pr_id: str = "0", tr_id: str = "0", zs_id: str = "0", fosa_id: str = "0"):

    d = date.today()
    today = d.strftime("%d_%m_%Y")
    yesterday = (d - timedelta(days=1)).strftime("%d_%m_%Y")
    today_path = 'data/internal_completeness_' + today + '.csv'
    yesterday_path = 'data/internal_completeness_' + yesterday + '.csv'
    data = []
    if (os.path.exists(today_path)):
        data = pd.read_csv(today_path)
    else:
        data = pd.read_sql(
            "SELECT * FROM ihris_mig.internal_completeness", mysql_get_mydb())
        data.to_csv(today_path, index=False)
        if (os.path.exists(yesterday_path)):
            os.remove(yesterday_path)

    if fosa_id != "0":
        data = data.loc[data['facility'] == fosa_id]
    elif zs_id != "0":
        data = data.loc[data['health_area_id'] == zs_id]
    elif tr_id != "0":
        data = data.loc[data['county'] == tr_id]
    elif pr_id != "0":
        data = data.loc[data['district'] == pr_id]
    data = data.drop_duplicates(subset=['id'])
    return data


def get_timesheet_data(pr_id: str = "0", tr_id: str = "0", zs_id: str = "0", fosa_id: str = "0"):

    d = date.today()
    today = d.strftime("%d_%m_%Y")
    yesterday = (d - timedelta(days=1)).strftime("%d_%m_%Y")
    today_path = 'data/timesheet_' + today + '.csv'
    yesterday_path = 'data/timesheet_' + yesterday + '.csv'
    data = []
    #timesheet_data_file = 'data/timesheet.csv'
    if (os.path.exists(today_path)):
        data = pd.read_csv(today_path)
    else:

        # last_created_date = None
        # data_old = pd.DataFrame()
        # if (os.path.exists(timesheet_data_file)):

        #     data_old = pd.read_csv(timesheet_data_file)
        #     data_old['created'] = pd.to_datetime(data_old['created'], format="%Y-%m-%d %H:%M:%S")
        #     data_old.sort_values(by='created', ascending=False, inplace=True)
        #     last_created_date = datetime.strftime(data_old.loc[0]['created'], format="%Y-%m-%d %H:%M:%S")
       
        data = pd.read_sql(get_timesheet_query(''), mysql_get_mydb())
        data.to_csv(today_path, index=False)
        if (os.path.exists(yesterday_path)):
            os.remove(yesterday_path)
        
        # if not data_old.empty:

        #     data = pd.concat([data, data_old])

        # data = data.drop_duplicates(subset=['id'])

        # data['created'] = pd.to_datetime(data['created'], format="%Y-%m-%d %H:%M:%S")
        # data.sort_values(by='created', ascending=False, inplace=True)
        # data = data.reset_index(drop=True)
        # data.to_csv(timesheet_data_file, index=False)
        

    if fosa_id != "0":
        data = data.loc[data['facility'] == fosa_id]
    elif zs_id != "0":
        data = data.loc[data['health_area'] == zs_id]
    elif tr_id != "0":
        data = data.loc[data['county'] == tr_id]
    elif pr_id != "0":
        data = data.loc[data['district'] == pr_id]
    return data


def getHealthAreaBaseListe(db: Session, pr_id: str,  zs_id:str, staff:int, groupData: dict):
    if zs_id != "0":
        data = db.query(HealthAreaBaseListe.ha_id, HealthAreaBaseListe.value,HealthArea.name).where(HealthAreaBaseListe.ha_id == zs_id).join(HealthArea, HealthArea.id == HealthAreaBaseListe.ha_id).distinct(HealthAreaBaseListe.ha_id).first()
        print(data[0])
        return [
            {
                'id': data[0],
                'name': data[2],
                'value': staff,
                'base_list_max':data[1],
                'progression': (staff/ data[1]*100) if data[1] > 0 else 0
            }
        ]
   
    data = db.query(HealthAreaBaseListe.ha_id, HealthAreaBaseListe.value).where(HealthAreaBaseListe.parent == pr_id).distinct(HealthAreaBaseListe.ha_id).all()
   
   
    response = []
    for index,elt in  groupData.items(): 
        eltfound = False
        for haData  in data :
           if haData.ha_id == index[1]:
                response.append({
                    'id':index[1],
                    'name': index[0],
                    'value': elt,
                    'base_list_max': haData.value,
                    "progression": (elt/ haData.value *100) if haData.value > 0 else 0
                })
                eltfound = True
                break
        if eltfound == False :
            response.append({
                'id':index[1],
                'name': index[0],
                'value': elt,
                'base_list_max':0,
                'progression': 0
            })
    return  response