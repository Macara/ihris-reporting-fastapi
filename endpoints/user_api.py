from fastapi import APIRouter
from datetime import datetime, timedelta
from typing import Annotated
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext
from pydantic import BaseModel
from sqlalchemy.orm import Session
from config.Database2 import SessionLocal

from models.models import PersonValidator
from models.usercrud import _get_access_facility, _login


# to get a string like this run:
# openssl rand -hex 32

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 480

fake_users_db = {
    "ihris-reporting": {
        "username": "ihris-reporting",
        "full_name": "IHRIS Reporting",
        "hashed_password": "$2b$12$4qzbD.4iqbjeP/ET5ICNeeTwsYxwKx4YlUGom4YE3Ft/WSEOv3TEy",
        "disabled": False,
    },
    "usimamizi": {
        "username": "usimamizi",
        "full_name": "USIMAMIZI KC",
        "hashed_password": "$2b$12$iyGsXMwdD5kh86HA22d94utrv2Nk.AcM4xojz8/5FKmgmL2JSExyq",
        "disabled": False,
    }
}

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None


class User(BaseModel):
    username: str
    full_name: str | None = None
    disabled: bool | None = None


class UserInDB(User):
    hashed_password: str


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def get_user(db, username: str):
    if username in db:
        user_dict = db[username]
        return UserInDB(**user_dict)


def authenticate_user(fake_db, username: str, password: str):
    user = get_user(fake_db, username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(token: Annotated[str, Depends(oauth2_scheme)]):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        usertype = payload.get("type")
        if usertype is not None:
            return username

        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception

    user = get_user(fake_users_db, username=token_data.username)
    if user is None:
        raise credentials_exception
    return user


async def get_current_active_user(
    current_user: Annotated[User, Depends(get_current_user)]
):
   # if current_user.disabled:
    #    raise HTTPException(status_code=400, detail="Inactive user")
    return current_user

api_user_router = APIRouter()


@api_user_router.post("/token", response_model=Token, tags=['users'],)
async def login_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()]
):

    user = authenticate_user(
        fake_users_db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
   
    return {"access_token": access_token, "token_type": "bearer", "validator": validator}


@api_user_router.get("/users/me/", response_model=User)
async def read_users_me(
    current_user: Annotated[User, Depends(get_current_active_user)]
):
    return current_user


@api_user_router.post('/users/reporting/login')
def repporting_login(username: str, password: str, db: Session = Depends(get_db)):

    user = _login(username=username, password=password, db=db)
    print(user)
    if user != None:

        user_access_facility = _get_access_facility(
            user_name=user.username, db=db)
        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = create_access_token(
            data={"sub": user.username, "type": "reporting"}, expires_delta=access_token_expires,
        )

        validatorData  =  db.query(PersonValidator).where(
        PersonValidator.username == user.username).first()
        validator = 0
        if validatorData != None:
            validator = 1
        return {
            'username': user.username,
            "token":  access_token,
            'access': user_access_facility,
            "validator": validator,
        }
    else:
         raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
        )
       # response.status_code = 404
        #return {'message': "Echec d'authentification"}
