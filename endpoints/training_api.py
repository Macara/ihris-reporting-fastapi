from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from config.Database2 import SessionLocal
from models.models import Cadre, Classification, County, District, Facility, HealthArea, Job
from models.training import dashboard_data, scheduled_training_course

router = APIRouter(
    prefix="/training",
)
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@router.get('/dashboard')
def dashboard(pr_id: str = "0", tr_id: str = "0", zs_id: str = "0", fosa_id: str = "0",  db: Session = Depends(get_db)):
    return dashboard_data(db=db)  

@router.get('/trainingcours')
def training_course(db: Session = Depends(get_db)):
    return scheduled_training_course(db=db)