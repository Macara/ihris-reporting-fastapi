from datetime import datetime
import json
from fastapi import APIRouter, Depends, Depends, Response
from jinja2 import Environment, FileSystemLoader
from helpers.helpers import download_dataframe_as_xlsx
from models.crud import (get_person_timesheet, get_revenu_report_utils)
from sqlalchemy.orm import Session
from config.Database2 import SessionLocal
from reports.reports import completude_timesheet, get_dashboard_data, get_salaire_prime_report, performance_bonus_calc, revenvus_report_data
from typing import List
from fastapi import Query
from reports.reports import get_internal_completeness_report_career
from weasyprint import HTML


router = APIRouter()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.get('/dashboard')
def dashboard_data(pr_id: str = "0", tr_id: str = "0", zs_id: str = "0", fosa_id: str = "0",  db: Session = Depends(get_db),):

    return get_dashboard_data(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id, db= db
    )


''' Person timesheet'''


@router.get("/person_timesheet")
def person_timesheet(person_id: str, db: Session = Depends(get_db)):

    return get_person_timesheet(
        person_id=person_id,
        db=db
    )

''' TimesSheet report'''

@router.get("/timesheet_report")
def times_report(pr_id: str = "0", tr_id: str = "0", zs_id: str = "0",
                 fosa_id: str = "0", zs_filter: int = 0,
                 start_date: str = None, end_date: str = None, download: int = 0, title: str = "",
                 
                 ):

    res, intervals, subtotal = completude_timesheet(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id,
        zs_filter=zs_filter, start_date=start_date, end_date=end_date
    )
     
    if download == 1 or download == 2:
        res = completude_timesheet(
            pr_id=pr_id, tr_id=tr_id,
             zs_id=zs_id, fosa_id=fosa_id,
            zs_filter=zs_filter,
            start_date=start_date,
            end_date=end_date, 
            download_details=download
        )
        return download_dataframe_as_xlsx(
            df=res,
            filename=f'Rapport de prestation - {title}'
        )
    #  Download  PDF  
    if download == 3 : 
        env = Environment(loader=FileSystemLoader("templates"))
        template = env.get_template("timesheet_report.html")
        _time = datetime.now().strftime( "%d-%m-%Y %H:%M:%S")
        res['_index'] = range(1, len(res) + 1)
        rendered_html = template.render(
            data=res,
            dates=intervals,
            current_date= _time,
            title= title,
            subtotal = subtotal
        )
        
        pdf_content = HTML(string=rendered_html).write_pdf()
        return Response(content=pdf_content, media_type="application/pdf", 
            headers={
                'Access-Control-Expose-Headers': 'Content-Disposition',
                'Content-Disposition': f'attachment; filename="COMPLETUDE DE PRESTATION -{title}-{_time} .pdf"'
            }
        )
    return {
        'report': json.loads(res.to_json(orient="table")),
        'intervals': intervals
    }


''' Revenus report'''


@router.get("/revenu_report")
def revenu_report(
    pr_id: str = "0", tr_id: str = "0", zs_id: str = "0",
    fosa_id: str = "0", zs_filter: int = 0,
    start_date: str = None, end_date: str = None,
    report_type: int = 0,
    cadres_ids: List[str] = Query(None),
    jobs_ids: List[str] = Query(None),
    class_ids: List[str] = Query(None),
    select_option:  List[str] = Query(None),
    download: int = 0,
    title: str = None,
):

    select_option = json.loads(select_option[0])
    cadres_ids = json.loads(cadres_ids[0])
    jobs_ids = json.loads(jobs_ids[0])
    class_ids = json.loads(class_ids[0])
    res, intervals = revenvus_report_data(
        pr_id=pr_id, tr_id=tr_id,
        zs_id=zs_id, fosa_id=fosa_id,
        report_type=report_type,
        zs_filter=zs_filter,
        start_date=start_date,
        end_date=end_date,
        cadres_ids=cadres_ids,
        jobs_ids=jobs_ids,
        class_ids=class_ids,
        select_option=select_option
    )
    if download == 1:
        s = ' '.join(select_option) + ' ' + ' '.join(cadres_ids) + \
            ' '.join(jobs_ids) + ' ' + ' '.join(class_ids)
        return download_dataframe_as_xlsx(
            df=res,
            filename=f'Revenus -{title} - {s}')
    return {
        'report': json.loads(res.to_json(orient="table")),
        'intervals': intervals
    }


@router.get("/comp/internal")
def read_root(pr_id: str = "0", tr_id: str = "0", zs_id: str = "0", fosa_id: str = "0", filter: int = 0):
    a = get_internal_completeness_report_career(
        pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id, filter=filter)
    return json.loads(a.to_json(orient="table"))


@router.get("/revenu_report_utils")
def revenu_report_utils(db: Session = Depends(get_db)):
    return get_revenu_report_utils(db=db)


@router.get("/performance_bonus_calc")
def bonus_report(pr_id: str = "0", tr_id: str = "0", zs_id: str = "0",
                 fosa_id: str = "0", zs_filter: int = 0,
                 start_date: str = None, end_date: str = None, download: int = 0, title: str = ""):

    res, intervals, subtotal = performance_bonus_calc(
        pr_id="district|748", tr_id="0", zs_id="0", fosa_id="0",
        zs_filter=1, start_date=start_date, end_date=end_date,
        download=download
    )
    if download == 1 : 
        env = Environment(loader=FileSystemLoader("templates"))
        template = env.get_template("performance_bonus.html")
        _time = datetime.now().strftime( "%d-%m-%Y %H:%M:%S")
        rendered_html = template.render(
            data=res,
            dates=intervals,
            subtotal= subtotal,current_date= _time
        )
        pdf_content = HTML(string=rendered_html).write_pdf()
        return Response(content=pdf_content, media_type="application/pdf",
             headers={
             'Access-Control-Expose-Headers': 'Content-Disposition',
             'Content-Disposition': f'attachment; filename="PRIME DE PERFORMANCE-{_time}.pdf"'
             }
        )
    return {
        'report': json.loads(res.to_json(orient="table")),
        'intervals': intervals,
        'subdata': json.loads(subtotal.to_json())
    }

@router.get("/situation_salaire_prime")
def salaire_prime_report(pr_id: str = "0", tr_id: str = "0", zs_id: str = "0",
                 fosa_id: str = "0", zs_filter: int = 0, title:str ="", download:int = 0):

    res =  get_salaire_prime_report(pr_id=pr_id, tr_id=tr_id, zs_id=zs_id, fosa_id=fosa_id, zs_filter=zs_filter)
    if download  ==  1: 
        return download_dataframe_as_xlsx(
            df=res,
            filename=f'SITUATION SALAIRE PRIME - {title}'
        )
    return {
        'report': json.loads(res.to_json(orient="table")),
    }