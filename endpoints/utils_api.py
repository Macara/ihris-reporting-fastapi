from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from config.Database2 import SessionLocal
from models.models import Cadre, Classification, County, District, Facility, HealthArea, Job

router = APIRouter(
    
)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@router.get('/cadres')
def cadre(db: Session = Depends(get_db)):
    return db.query(Cadre).where(
        Cadre.i2ce_hidden == 0).order_by(Cadre.name.asc()).all()

@router.get('/jobs')
def job(db: Session = Depends(get_db)):
    return db.query(Job).where(
        Job.i2ce_hidden == 0).order_by(Job.title.asc()).all()

@router.get('/classifications')
def classification(db: Session = Depends(get_db)):
    return db.query(Classification).where(
        Classification.i2ce_hidden == 0).order_by(Classification.name.asc()).all()

