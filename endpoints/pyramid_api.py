from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from config.Database2 import SessionLocal
from models.models import County, District, Facility, HealthArea

router = APIRouter(
    prefix="/pyramid",
)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.get('/district/{country_id}')
def district(filter: int = 1, db: Session = Depends(get_db)):
    if (filter == 1):
        return db.query(District).where(
            District.i2ce_hidden == 0).where(
            District.id.in_((
                'district|702', 'district|748', 'district|752', 'district|750',
                'district|812', 'district|703', 'district|816', 'district|757',
                'district|805', 'district|806', 'district|807', 'district|813'
                )
            )
        ).all()
    return db.query(District).where(District.i2ce_hidden == 0).all()


@router.get('/county/{district_id}')
def county(
        district_id: str,
        db: Session = Depends(get_db)):
    return db.query(County).where(
        County.i2ce_hidden == 0).where(
        County.district == district_id).all()


@router.get('/health_area/{county_id}')
def Health_area(
        county_id: str,
        db: Session = Depends(get_db)):
    return db.query(HealthArea).where(
        HealthArea.i2ce_hidden == 0).where(
        HealthArea.county == county_id).all()


@router.get('/facility/{health_area_id}')
def facilities(
        health_area_id: str,
        db: Session = Depends(get_db)):
    return db.query(Facility).where(
        Facility.i2ce_hidden == 0).where(
        Facility.location == health_area_id).all()
