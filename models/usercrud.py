from sqlalchemy.orm import Session
from models.models import AccessFacility, County, Country, District, Facility, HealthArea, User
import hashlib
from endpoints.pyramid_api import county, facilities

 


def _login(db: Session, username: str, password: str):
    return db.query(User).where(
        User.username == username).where(
        User.password == hashlib.md5(str(password).encode('utf-8')).hexdigest()
    ).first()


def _get_access_facility(db: Session, user_name: str):

    access_facilitly = db.query(AccessFacility).where(
        AccessFacility.parent == 'user|' + user_name
    ).first()

    access_facility_type = ""
    af_list = None
    target = 0
    facility_parents = [];
    
    if access_facilitly != None:
        if 'facility' in access_facilitly.location:
            af_list = db.query(Facility).where(
                Facility.id == access_facilitly.location).first()
            access_facility_type = 'none'
            target = 0

            healthArea = db.query(HealthArea).where(HealthArea.id == af_list.location).first()
            facility_parents.append(healthArea)

            county = db.query(County).where(County.id == healthArea.county).first()
            facility_parents.append(county)

            district = db.query(District).where(District.id == county.district).first()
            facility_parents.append(district)

        elif "health_area" in access_facilitly.location:
            af_list = db.query(HealthArea).where(
                HealthArea.id == access_facilitly.location).first()
            access_facility_type = 'facility'
            target = 1

            county = db.query(County).where(County.id == af_list.county).first()
            facility_parents.append(county)

            district = db.query(District).where(District.id == county.district).first()
            facility_parents.append(district)


        elif "county" in access_facilitly.location:
            af_list = db.query(County).where(
                County.id == access_facilitly.location).first()
            access_facility_type = 'health_area'
            target = 2

            district = db.query(District).where(District.id == af_list.district).first()
            facility_parents.append(district)

        elif "district" in access_facilitly.location:
            af_list = db.query(District).where(
                District.id == access_facilitly.location).first()
            access_facility_type = 'county'
            target = 3

        elif "country" in access_facilitly.location  or 'region' in access_facilitly.location:
            af_list = db.query(Country).where(
                Country.id == "country|CD").first()
            access_facility_type = 'district'
            target = 4
        

    return {
        "_access_facilitly": access_facilitly,
        "access_facilitly": af_list,
        'access_facility_type': target,
        'access_facility_target': access_facility_type,
        'facility_parents': facility_parents
    }



