import fastapi


def get_timesheet_query(datestr: str):

    subquery = f" order by hippo_person_timesheet.created desc  limit 145018"
    if datestr != '':
        subquery = f" where date(hippo_person_timesheet.created) >= '{datestr}' order by hippo_person_timesheet.created asc  limit 4000"

    return timesheet_query + subquery


timesheet_query = """
    select  distinct
    CONVERT(hippo_person_timesheet.id, char(200)) as id , 
    CONVERT( hippo_person_timesheet.created, char(200)) as created , 
    CONVERT( hippo_person_timesheet.parent, char(200)) as parent,
    upper(trim(CONVERT( 
    CONCAT(COALESCE(hippo_person.firstname,"")," ", COALESCE(hippo_person.surname,"")," ", COALESCE(hippo_person.othername,"")) , CHAR(200)))) as fullname,
    CONVERT( cast(
    substring(hippo_person_timesheet.salaire_recu, position('=' in hippo_person_timesheet.salaire_recu ) + 1, length(hippo_person_timesheet.salaire_recu))
    as decimal(12)
    ), char(200)) as 'salaire_recu',
    CONVERT( cast(
    substring(hippo_person_timesheet.prime_risque, position('=' in hippo_person_timesheet.prime_risque ) + 1, length(hippo_person_timesheet.prime_risque))
        as decimal(12)
    ), char(200)) as 'prime_risque' ,
    CONVERT( cast(
    substring(hippo_person_timesheet.prime_locale, position('=' in hippo_person_timesheet.prime_locale ) + 1, length(hippo_person_timesheet.prime_locale))
    as decimal(12)
    ), char(200)) as 'prime_locale',
    CONVERT(	cast(
    substring(hippo_person_timesheet.prime_ptf, position('=' in hippo_person_timesheet.prime_ptf ) + 1, length(hippo_person_timesheet.prime_ptf))
    as decimal(12)
    ), char(200)) as 'prime_ptf' 
    ,
        CONVERT( hippo_person_timesheet.jours_prestes,char(200)) as jours_prestes,
        CONVERT(  hippo_person_timesheet.mois_annee, char(200)) as mois_annee,
        CONVERT(hippo_employment_status_rdc.cadre, char(200)) as cadre,
        CONVERT(hippo_employment_status_rdc.job, char(200) )as job,
        CONVERT(hippo_employment_status_rdc.prime, char(20))as prime,
         CONVERT(hippo_employment_status_rdc.salaire, char(20))as salaire,

        CONVERT(hippo_employment_status_rdc.classification, char(200)) as classification,
        CONVERT(hippo_employment_status_rdc.salary_grade, char(200)) as salary_grade,
        CONVERT(hippo_facility.id, char(200)) as  facility,
        CONVERT(hippo_facility.name, char(200)) as  facility_name,
        CONVERT(hippo_health_area.id, char(200))as  health_area,
        CONVERT(hippo_health_area.name, char(200)) as  health_area_name,
        CONVERT(hippo_county.id , char(200)) as county,
        CONVERT(hippo_county.name, char(200))as county_name,
        CONVERT(hippo_county.district, char(200)) as  district,
        CONVERT(hippo_district.name, char(200)) as  district_name
        
    from  
        ihris_mig.hippo_person_timesheet 
        join hippo_employment_status_rdc  on  hippo_employment_status_rdc.parent  =  hippo_person_timesheet.parent 
        left join hippo_facility on hippo_facility.id  =  hippo_employment_status_rdc.facility
        left join  hippo_health_area on hippo_health_area.id =  hippo_facility.location
        left join hippo_county on hippo_county.id = hippo_health_area.county
        left join hippo_district on hippo_district.id = hippo_county.district
        left join hippo_person on hippo_person.id = hippo_person_timesheet.parent
"""

def person_file_query(id ):
   a =   """
    select hippo_person.firstname,
        hippo_person.id,
		hippo_person.othername,
        hippo_person.surname,
        hippo_country.name as  nationality,
        hippo_county.name as residence,
		hippo_degree.name as degree,
        hippo_person_id.id_num,
        hippo_person_id.created as id_created,
        hippo_id_type.name as id_type,
        hippo_demographic.birth_date,
        hippo_demographic. gender,
        hippo_marital_status.name as marital_status,
        hippo_demographic.dependents,
        hippo_person_photo_passport.id as image,
        
        hippo_person_contact_personal.address as contact_personal_address,
        hippo_person_contact_personal.mobile_phone as contact_personal_mobile_phone,
        hippo_person_contact_personal.alt_telephone as contact_personal_alt_telephone,
        hippo_person_contact_personal.email as contact_personal_email,
        
		hippo_person_contact_work.address as contact_work_address,
        hippo_person_contact_work.mobile_phone as contact_work_mobile_phone,
        hippo_person_contact_work.alt_telephone as contact_work_alt_telephone,
        hippo_person_contact_work.email as contact_work_email,

        person_validation.date as validation_date,
        person_validation.id as validation_id,
        person_validation.code as validation_code

        from hippo_person
        left join hippo_degree on hippo_degree.id = hippo_person.degree
		left join hippo_person_id on hippo_person_id.parent = hippo_person.id
        left join hippo_id_type on hippo_id_type.id = hippo_person_id.id_type
        left join hippo_demographic on hippo_demographic.parent = hippo_person.id
        left join hippo_marital_status on hippo_marital_status.id  = hippo_demographic.marital_status
        left join hippo_person_contact_personal on hippo_person_contact_personal.parent = hippo_person.id
		left join hippo_person_contact_work on hippo_person_contact_work.parent = hippo_person.id
        left join hippo_person_photo_passport on hippo_person_photo_passport.parent = hippo_person.id
        left join hippo_county on hippo_county.id = hippo_person.residence
        left join hippo_country on hippo_country.id = hippo_person.nationality
        left join person_validation on person_validation.parent = hippo_person.id
 
        where hippo_person.id =
    """
   a = a + "'" + id +"'"
   return a

def person_file_employments(id):
    q = """
        select 
	hippo_employment_status_rdc.identifie,
    hippo_employment_status_rdc.prime,
    hippo_employment_status_rdc.salaire,
    hippo_employment_status_rdc.ref_engagement,
    hippo_employment_status_rdc.ref_on_employment,
    hippo_employment_status_rdc.year_of_appointment,
    hippo_cadre.name as cadre,
     hippo_job.title as job,
    hippo_classification.name as classification,
    hippo_employee_status.name as employee_status,
    hippo_salary_grade.name as salary_grade,
    hippo_facility.name as facility,
    hippo_health_area.name as health_area,
    hippo_district.name as district
	from hippo_employment_status_rdc
    left join hippo_cadre on hippo_cadre.id = hippo_employment_status_rdc.cadre
    left join hippo_job on hippo_job.id = hippo_employment_status_rdc.job
    left join hippo_classification on hippo_classification.id = hippo_employment_status_rdc.classification
    left join hippo_employee_status on hippo_employee_status.id = hippo_employment_status_rdc.employee_status
    left join hippo_salary_grade on hippo_salary_grade.id = hippo_employment_status_rdc.salary_grade
    left join hippo_facility on hippo_facility.id = hippo_employment_status_rdc.facility
    left join hippo_health_area on hippo_health_area.id =  hippo_facility.location
    left join hippo_county on hippo_county.id = hippo_health_area.county
    left join hippo_district on hippo_district.id = hippo_county.district

    where hippo_employment_status_rdc.parent= 
    """
    q = q + "'" + id +"' order by hippo_employment_status_rdc.created asc"
    return q